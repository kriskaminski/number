/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    castToNumber,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class NumberCondition extends ConditionBlock<{
    mode: TMode;
    value?: number;
    to?: number;
}> {
    @condition
    verify(): boolean {
        const numberSlot = this.valueOf<number>();

        if (numberSlot) {
            switch (this.props.mode) {
                case "equal":
                    return (
                        numberSlot.hasValue &&
                        numberSlot.value === this.props.value
                    );
                case "below":
                    return (
                        numberSlot.hasValue &&
                        numberSlot.value < castToNumber(this.props.value)
                    );
                case "above":
                    return (
                        numberSlot.hasValue &&
                        numberSlot.value > castToNumber(this.props.value)
                    );
                case "between":
                    return (
                        numberSlot.hasValue &&
                        numberSlot.value >= castToNumber(this.props.value) &&
                        numberSlot.value <= castToNumber(this.props.to)
                    );
                case "defined":
                    return numberSlot.hasValue;
                case "undefined":
                    return !numberSlot.hasValue;
            }
        }

        return false;
    }
}
