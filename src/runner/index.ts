/** Dependencies */
import { NodeBlock, Slots, assert } from "tripetto-runner-foundation";
import "./condition";

export abstract class Number extends NodeBlock {
    /** Contains the number slot with the value. */
    readonly numberSlot = assert(this.valueOf<number, Slots.Numeric>("number"));

    /** Contains if the block is required. */
    readonly required = this.numberSlot.slot.required || false;
}
