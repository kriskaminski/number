/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { TMode } from "../runner/mode";
import { Number } from "./";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Number,
    icon: ICON,
    get label() {
        return pgettext("block:number", "Verify number");
    },
})
export class NumberCondition extends ConditionBlock {
    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number;

    @definition
    @affects("#name")
    to?: number;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "between":
                    return `\`${this.slot.toString(this.value)}\` ≤ @${
                        this.slot.id
                    } ≤ \`${this.slot.toString(this.to)}\``;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:number",
                        "specified"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:number",
                        "not specified"
                    )}`;
                case "above":
                case "below":
                case "equal":
                    return `@${this.slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${`\`${this.slot.toString(this.value)}\``}`;
            }
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        const slot = this.slot as Slots.Numeric | undefined;

        const valueTo = new Forms.Numeric(
            Forms.Numeric.bind(this, "to", undefined, 0)
        )
            .precision((slot && slot.precision) || 0)
            .visible(this.mode === "between");

        const group = new Forms.Group([
            new Forms.Numeric(Forms.Numeric.bind(this, "value", undefined, 0))
                .precision((slot && slot.precision) || 0)
                .decimalSign((slot && slot.decimal) || "")
                .thousands(
                    slot && slot.separator ? true : false,
                    (slot && slot.separator) || ""
                )
                .prefix((slot && slot.prefix) || "")
                .prefixPlural((slot && slot.prefixPlural) || "")
                .suffix((slot && slot.suffix) || "")
                .suffixPlural((slot && slot.suffixPlural) || "")
                .autoFocus(),
            valueTo,
        ]).visible(this.mode !== "defined" && this.mode !== "undefined");

        this.editor.form({
            title: pgettext("block:number", "When number:"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext("block:number", "Is equal to"),
                            value: "equal",
                        },
                        {
                            label: pgettext("block:number", "Is lower than"),
                            value: "below",
                        },
                        {
                            label: pgettext("block:number", "Is higher than"),
                            value: "above",
                        },
                        {
                            label: pgettext("block:number", "Is between"),
                            value: "between",
                        },
                        {
                            label: pgettext("block:number", "Is specified"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:number", "Is not specified"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    group.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    valueTo.visible(mode.value === "between");
                }),
                group,
            ],
        });
    }
}
