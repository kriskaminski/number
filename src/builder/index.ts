/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    conditions,
    each,
    editor,
    isNumber,
    isString,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { NumberCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:number", "Number");
    },
})
export class Number extends NodeBlock {
    numberSlot!: Slots.Numeric;

    @slots
    defineSlot(): void {
        this.numberSlot = this.slots.static({
            type: Slots.Numeric,
            reference: "number",
            label: Number.label,
        });
    }

    @editor
    defineEditor(): void {
        const minimum = new Forms.Numeric(
            Forms.Numeric.bind(this.numberSlot, "minimum", undefined)
        )
            .precision(this.numberSlot.precision || 0)
            .label(pgettext("block:number", "Minimum"));
        const maximum = new Forms.Numeric(
            Forms.Numeric.bind(this.numberSlot, "maximum", undefined)
        )
            .precision(this.numberSlot.precision || 0)
            .label(pgettext("block:number", "Maximum"));

        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:number", "Format"),
            form: {
                title: pgettext("block:number", "Format"),
                controls: [
                    new Forms.Dropdown(
                        [
                            { label: "#", value: 0 },
                            { label: "#.#", value: 1 },
                            { label: "#.##", value: 2 },
                            { label: "#.###", value: 3 },
                            { label: "#.####", value: 4 },
                            { label: "#.#####", value: 5 },
                            { label: "#.######", value: 6 },
                            { label: "#.#######", value: 7 },
                            { label: "#.########", value: 8 },
                        ],
                        Forms.Dropdown.bind(
                            this.numberSlot,
                            "precision",
                            undefined
                        )
                    ).on((precision: Forms.Dropdown<number>) => {
                        minimum.precision(precision.value || 0);
                        maximum.precision(precision.value || 0);
                    }),
                ],
            },
            activated: isNumber(this.numberSlot.precision),
        });

        this.editor.option({
            name: pgettext("block:number", "Signs"),
            form: {
                title: pgettext("block:number", "Signs"),
                controls: [
                    new Forms.Dropdown(
                        [
                            { label: "#.#", value: "." },
                            { label: "#,#", value: "," },
                        ],
                        Forms.Dropdown.bind(
                            this.numberSlot,
                            "decimal",
                            undefined
                        )
                    ).label(pgettext("block:number", "Decimal sign")),
                    new Forms.Dropdown(
                        [
                            {
                                label: pgettext("block:number", "None"),
                                value: undefined,
                            },
                            { label: "#,###", value: "," },
                            { label: "#.###", value: "." },
                        ],
                        Forms.Dropdown.bind(
                            this.numberSlot,
                            "separator",
                            undefined
                        )
                    ).label(pgettext("block:number", "Thousands separator")),
                    new Forms.Static(
                        pgettext(
                            "block:number",
                            "**Note:** These signs are used to format the number in de dataset. When the number is displayed in a runner, the appropriate user locale might be applied making it seem like changing these settings have no effect."
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.numberSlot.separator) ||
                isString(this.numberSlot.decimal),
        });

        this.editor.option({
            name: pgettext("block:number", "Limit values"),
            form: {
                title: pgettext("block:number", "Limit values"),
                controls: [minimum, maximum],
            },
            activated:
                isNumber(this.numberSlot.minimum) ||
                isNumber(this.numberSlot.maximum),
        });

        const prefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "prefix", undefined)
        )
            .sanitize(false)
            .on((p: Forms.Text) => {
                minimum.prefix((p.isFeatureEnabled && p.value) || "");
                maximum.prefix((p.isFeatureEnabled && p.value) || "");
            });
        const pluralPrefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "prefixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((p: Forms.Text) => {
                minimum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) || ""
                );
                maximum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) || ""
                );
            })
            .placeholder(
                pgettext("block:number", "Prefix when value is plural")
            )
            .visible(isString(this.numberSlot.prefixPlural));

        this.editor.option({
            name: pgettext("block:number", "Prefix"),
            form: {
                title: pgettext("block:number", "Prefix"),
                controls: [
                    prefix,
                    new Forms.Checkbox(
                        pgettext(
                            "block:number",
                            "Specify different prefix for plural values"
                        ),
                        isString(this.numberSlot.prefixPlural)
                    ).on((c) => {
                        prefix.placeholder(
                            (c.isChecked &&
                                pgettext(
                                    "block:number",
                                    "Prefix when value is singular"
                                )) ||
                                ""
                        );

                        pluralPrefix.visible(c.isChecked);
                    }),
                    pluralPrefix,
                ],
            },
            activated: isString(this.numberSlot.prefix),
        });

        const suffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "suffix", undefined)
        )
            .sanitize(false)
            .on((s: Forms.Text) => {
                minimum.suffix((s.isFeatureEnabled && s.value) || "");
                maximum.suffix((s.isFeatureEnabled && s.value) || "");
            });
        const pluralSuffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.numberSlot, "suffixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((s: Forms.Text) => {
                minimum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) || ""
                );
                maximum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) || ""
                );
            })
            .placeholder(
                pgettext("block:number", "Suffix when value is plural")
            )
            .visible(isString(this.numberSlot.suffixPlural));

        this.editor.option({
            name: pgettext("block:number", "Suffix"),
            form: {
                title: pgettext("block:number", "Suffix"),
                controls: [
                    suffix,
                    new Forms.Checkbox(
                        pgettext(
                            "block:number",
                            "Specify different suffix for plural values"
                        ),
                        isString(this.numberSlot.suffixPlural)
                    ).on((c) => {
                        suffix.placeholder(
                            (c.isChecked &&
                                pgettext(
                                    "block:number",
                                    "Suffix when value is singular"
                                )) ||
                                ""
                        );

                        pluralSuffix.visible(c.isChecked);
                    }),
                    pluralSuffix,
                ],
            },
            activated: isString(this.numberSlot.suffix),
        });

        this.editor.groups.options();
        this.editor.required(this.numberSlot);
        this.editor.visibility();
        this.editor.alias(this.numberSlot);
        this.editor.exportable(this.numberSlot);
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    mode: "equal" as "equal",
                    label: pgettext("block:number", "Number is equal to"),
                },
                {
                    mode: "below" as "below",
                    label: pgettext("block:number", "Number is lower than"),
                },
                {
                    mode: "above" as "above",
                    label: pgettext("block:number", "Number is higher than"),
                },
                {
                    mode: "between" as "between",
                    label: pgettext("block:number", "Number is between"),
                },
                {
                    mode: "defined" as "defined",
                    label: pgettext("block:number", "Number is specified"),
                },
                {
                    mode: "undefined" as "undefined",
                    label: pgettext("block:number", "Number is not specified"),
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: NumberCondition,
                    label: condition.label,
                    props: {
                        slot: this.numberSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}
